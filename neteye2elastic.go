package main

import (
	//	"crypto/sha256"
	//	"encoding/hex"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	//"net"
	"os"
	"os/signal"
	//	"path"
	//"strconv"
	//"strings"
	"sync"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/hashicorp/golang-lru"
	"github.com/oschwald/geoip2-golang"
	"github.com/vjeantet/grok"
	"golang.org/x/net/context"
	"gopkg.in/mcuadros/go-syslog.v2"
	"gopkg.in/olivere/elastic.v5"
)

type Request struct {
	Host        string `json:"host"`
	Policy      string `json:"policy"`
	Source_ip   string `json:"source_ip"`
	Source_port string `json:"source_port"`
	Dest_ip     string `json:"dest_ip"`
	Dest_port   string `json:"dest_port"`
	Protocol    string `json:"protocol"`
	Timestamp   string `json:"timestamp"`
	Action      string `json:"action"`
	Domain      string `json:"domain"`
}

type Config struct {
	Address        string
	Port           string
	Nodes          []string
	Index          string
	Workers        int
	Bulk           int
	Buffer         int
	Timeout        int
	Geoip          string
	Salt           string
	Grok           string
	Drop_dest_port string
}

type Worker struct {
	Work     chan string
	Indexer  *elastic.BulkService
	Esclient *elastic.Client
}

func NewWorker(work chan string, c *elastic.Client) Worker {
	indexer := c.Bulk()
	return Worker{
		Work:     work,
		Indexer:  indexer,
		Esclient: c}
}

func (w Worker) NewRequest(msg string) (Request, error) {
	g, _ := grok.New()
	values, _ := g.Parse(config.Grok, msg)
	var request Request

	if values["dest_port"] == config.Drop_dest_port {
		err := errors.New("Drop some log: Dest_Port " + config.Drop_dest_port)
		return request, err
	}

	request.Policy = values["policy"]
	request.Dest_ip = values["dest_ip"]
	request.Dest_port = values["dest_port"]
	request.Source_ip = values["source_ip"]
	request.Source_port = values["source_port"]

	request.Protocol = values["Protocol"]
	request.Action = values["action"]
	request.Domain = values["domain"]

	/*
		if host := captures["URIHOST"][0]; host != "www.google.com" {
			t.Fatal("URIHOST should be www.google.com")
		}

		msgparts := strings.Split(msg, " || ")
		var request Request
		if len(msgparts) != 11 {
			err := errors.New("Error parsing raw message: " + msg)
			return request, err
		}
		request.Client = msgparts[0]
		request.Method = msgparts[1]
		request.Host = msgparts[2]
		request.Uri = msgparts[3]
		if s, err := strconv.Atoi(msgparts[4]); err == nil {
			request.Status = s
		}
		// requests without valid status code will be ignored. Happens when we use http::retry.
		if request.Status < 100 || request.Status > 511 {
			err := errors.New("Error invalid status code: " + strconv.Itoa(request.Status) + " : " + request.Host + request.Uri)
			return request, err
		}
		if s, err := strconv.Atoi(msgparts[5]); err == nil {
			request.Contentlength = s
		}
		request.Referer = msgparts[6]
		request.Useragent = msgparts[7]
		request.Node = msgparts[8]
		request.Pool = path.Base(msgparts[9])
		request.Virtual = path.Base(msgparts[10])
		// use LRU cache
	*/
	/*
		var record *geoip2.City
		if val, ok := geocache.Get(request.Client); ok {
			record = val.(*geoip2.City)
		} else {
			ip := net.ParseIP(request.Client)
			var err error
			record, err = geodb.City(ip)
			if err != nil {
				log.Printf("Error parsing client ip %s: %s\n", request.Client, err)
			}
			geocache.Add(request.Client, record)
		}
		if record.Location.Longitude != 0 && record.Location.Latitude != 0 {
			request.City = record.City.Names["en"]
			request.Country = record.Country.Names["en"]
			request.Location = strconv.FormatFloat(record.Location.Latitude, 'f', 6, 64) + "," + strconv.FormatFloat(record.Location.Longitude, 'f', 6, 64)
		}
	*/

	request.Timestamp = time.Now().UTC().Format("2006-01-02T15:04:05Z")
	/*
		if config.Salt != "" {
			// hash client ip with secret salt.
			if val, ok := hashcache.Get(request.Client); ok {
				request.Client = val.(string)
			} else {
				h := sha256.New()
				h.Write([]byte(request.Client + config.Salt))
				hexhash := hex.EncodeToString(h.Sum(nil))[0:16]
				hashcache.Add(request.Client, hexhash)
				request.Client = hexhash
			}
		}
	*/
	return request, nil
}

func (w Worker) Start() {
	go func() {
		wg.Add(1)
		defer wg.Done()
		timer := time.After(time.Second * time.Duration(config.Timeout))
		ctx := context.Background()
		for {
			select {
			case <-timer:
				w.Indexer.Do(ctx)
				timer = time.After(time.Second * time.Duration(config.Timeout))
			case m, ok := <-w.Work:
				if !ok {
					w.Indexer.Do(ctx)
					return
				}
				request, err := w.NewRequest(m)
				if err != nil {
					log.Println(err)
					continue
				}
				reqIndex := elastic.NewBulkIndexRequest().Index(config.Index).Type("request").Id("").Doc(request)
				w.Indexer = w.Indexer.Add(reqIndex)
				// should be enough for now, but we can increase load by creating a group of workers
				if w.Indexer.NumberOfActions() >= config.Bulk {
					w.Indexer.Do(ctx)
					// reset timer
					timer = time.After(time.Second * time.Duration(config.Timeout))
				}
			}
		}
	}()
}

var wg sync.WaitGroup
var geocache *lru.Cache
var hashcache *lru.Cache
var geodb *geoip2.Reader
var config Config

func main() {
	configtoml := flag.String("c", "neteye2elastic.toml", "Path to config.")
	flag.Parse()
	file, err := ioutil.ReadFile(*configtoml)
	if err != nil {
		log.Fatal(err)
	}
	err = toml.Unmarshal(file, &config)
	if err != nil {
		log.Fatal("Problem parsing config: ", err)
	}
	// init syslog server
	channel := make(syslog.LogPartsChannel)
	handler := syslog.NewChannelHandler(channel)
	server := syslog.NewServer()
	server.SetFormat(syslog.RFC3164)
	server.SetHandler(handler)
	server.ListenUDP(config.Address + ":" + config.Port)
	server.ListenTCP(config.Address + ":" + config.Port)
	err = server.Boot()
	if err != nil {
		log.Fatal(err)
	}
	// init geoip2 database
	// http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz
	geodb, err = geoip2.Open(config.Geoip)
	if err != nil {
		log.Fatal(err)
	}
	defer geodb.Close()
	// init our lru cache of geodb
	geocache, _ = lru.New(100000)
	// init our lru cache of sha256 hashes
	hashcache, _ = lru.New(100000)
	// init our elastic client
	c, err := elastic.NewClient(
		elastic.SetURL(config.Nodes...),
		elastic.SetSniff(false),
		elastic.SetHealthcheckInterval(10*time.Second),
		elastic.SetMaxRetries(5))
	if err != nil {
		log.Fatal(err)
	}
	// A buffered channel that we can send work requests on.
	var workqueue = make(chan string, config.Buffer)
	// lets start our desired workers.
	for i := 0; i < config.Workers; i++ {
		worker := NewWorker(workqueue, c)
		worker.Start()
	}
	go func(channel syslog.LogPartsChannel) {
		for logParts := range channel {
			m := logParts["content"].(string)
			if m != "" {
				select {
				case workqueue <- m: // Put request in the channel unless it is full
				default:
					log.Println("Workqueue channel is full. Discarding request.")
				}
			}
		}
	}(channel)

	sigchan := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigchan, os.Interrupt)
	go func() {
		for _ = range sigchan {
			// stop our server in a graceful manner
			log.Println("Stopping Neteye2elastic ...")
			server.Kill()
			close(workqueue)
			wg.Wait()
			log.Println("Finished.")
			done <- true
		}
	}()
	log.Println("Starting Neteye2elastic ...")
	server.Wait()
	<-done
}
