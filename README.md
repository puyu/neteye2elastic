# neteye2elastic

A fast system tool for receiving high volumes of traffic logs sent from neusoft neteye firewall, then indexes these requests inside ES.

It is today battle tested in a production environment handling loads of 500k req/min on a single host.

Features

* Works with Elasticsearch 
* Support for clusters of ES nodes, for distribution of indexing load.
* Health checks for each ES node is enabled by default.
* Uses bulk indexing for better performance with large amount of requests.
* GeoIP lookup of each request, adds long/lat, city, and country for each client.
* Allows to obfuscate client ip if needed.
* ...

## Getting started

1. Enable the following irule on your virtual servers:

2. Download compiled binary from `https://github.com/hufuyu/neteye2elastic/releases` or `go get github.com/hufuyu/neteye2elastic`

3. Download the latest Maxmind GeoLite2-City or GeoIP2-City db.

4. Edit `neteye2elastic.toml` (check example in repo)

5. Apply the ES template inside the repo. (optional but recommended)

6. Run `neteye2elastic -c /path/to/neteye2elastic.toml`

7. Take a cup of coffee and make some nice dashboards inside Kibana :)

## Extra recommendations

Use [curator](https://www.elastic.co/guide/en/elasticsearch/client/curator/current/index.html) to create and rotate daily indexes.
